from selenium import webdriver
import os



PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
DRIVER_BIN = os.path.join(PROJECT_ROOT, "chromedriver")

driver = webdriver.Chrome(executable_path = DRIVER_BIN)
driver.get("https://the-internet.herokuapp.com/context_menu")
# bodyText = driver.find_elements_by_class_name("example")
content = driver.find_element_by_class_name('example')

print(content.text)
x="Right-click"
assert (x in content.text)

driver.close()
driver.quit()
