from bs4 import BeautifulSoup
import requests
import unittest

class TestFunctions(unittest.TestCase):

    def test_context_menu(self):
        resp = requests.get('https://the-internet.herokuapp.com/context_menu')
        soup = BeautifulSoup(resp.text, 'html.parser')
        context = soup.find_all('p')[1].get_text()
        expexted_string = "Right-click"
        self.assertIn(expexted_string, context)

if __name__ == '__main__':
    unittest.main()
